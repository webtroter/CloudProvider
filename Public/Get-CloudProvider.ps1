function Get-CloudProvider {
    <#
    .Synopsis
      Gets the registry key where the Cloud Provider is registered

    .Example
        C:\PS>Get-CloudProvider -Name "OneDrive"
        
        This example gets the OneDrive Cloud Provider registry keys
    .Notes
        Name: Get-CloudProvider
        Author: Alexis Vézina
        Last Edit: Date
    .Inputs
        None
    .Outputs
        None
    .PARAMETER Name
    The Name corresponds to the Name you see in the Windows Explorer

    #Requires -Version 2.0
    #>
    [CmdletBinding(SupportsShouldProcess=$True,DefaultParameterSetName="Default")]
    Param
    (
        [Parameter(Mandatory=$false,HelpMessage="Cloud Provide Name",ParameterSetName="Name")]
        [string]$Name,
        # Parameter help description
        [Parameter(Mandatory=$false,HelpMessage="Cloud Provider CLSID",ParameterSetName="CLSID")]
        [guid]$CLSID
    )
    PROCESS {
        if ($pscmdlet.ShouldProcess("Continue?")) {
            #Write-Output "Doing it..."
            
            #Find all the Cloud Providers
            $cloudProviders = Get-ChildItem -Path HKCU:\Software\Classes\CLSID\ | Where-Object {$_.GetValueNames() -eq "System.IsPinnedToNameSpaceTree"}
            
            if ($CLSID -ne $null -and $Name -eq "") {
                $cloudProvider = $cloudProviders | Where-Object {$_.PSChildName.Trim("{","}") -eq $CLSID}
            }
            else {
                foreach ($provider in $cloudProviders) {
                    if ( ( $(Get-ItemProperty $provider.PSPath)."(default)") -eq $Name ) {
                        $cloudProvider = $provider
                    }
                }
            }
            Write-Verbose $psCmdlet.ParameterSetName

            if ($psCmdlet.ParameterSetName -eq "Default") {
                Write-Verbose "Return All"
                Write-Output $cloudProviders
            }
            else {
                Write-Verbose "Return Single"
                Write-Output $cloudProvider
            }
            
        }
        else {
            Write-Output "Not doing it..."
        }
    }
} #End function