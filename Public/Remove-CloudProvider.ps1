function Remove-CloudProvider {
    <#
    .Synopsis
      Removes the registry key where the Cloud Provider is registered.
    .Example
        C:\PS>Remove-CloudProvider -Name MyFolder
        Removes the cloud provider "MyFolder"

    .Notes
        Name: Remove-CloudProvider
        Author: Alexis Vézina
        Last Edit: Date
    .Inputs
        None
    .Outputs
        None
    .PARAMETER Name
    The Name corresponds to the Name you see in the Windows Explorer

    #Requires -Version 2.0
    #>
    [CmdletBinding(SupportsShouldProcess=$True,DefaultParameterSetName="Default")]
    Param
    (
        [Parameter( Position=0,
            Mandatory=$false,
            HelpMessage="Cloud Provide Name",
            ParameterSetName="Name")]
        [string]$Name,
        # Parameter help description
        [Parameter( Mandatory=$false,
            HelpMessage="Cloud Provider CLSID",
            ParameterSetName="CLSID")]
        [guid]$CLSID
    )
    PROCESS {
        
        if ($pscmdlet.ShouldProcess("Continue?")) {
            #Write-Output "Doing it..."
            $cloudProvider
            if ($CLSID -eq $null) {
                $cloudProvider = Get-ChildItem -Path "HKCU:\Software\Classes\CLSID\" | Get-ItemProperty | Where-Object {$_."(default)" -eq $Name}
                $CLSID = $cloudProvider.PSChildName.Trim("{","}")
            }
            Write-Output "Removing Registry key"
            Get-ChildItem -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\SyncRootManager" | `
                Get-ItemProperty | Where-Object {$_."DisplayNameResource" -eq $Name} | Remove-Item -Recurse
            Remove-Item -Path "HKCU:\Software\Classes\CLSID\{$CLSID}" -Recurse
            Remove-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\NameSpace\{$CLSID}"
            Remove-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -Name "{$CLSID}"
            Write-Output "Removed"
        }
        else {
            #Write-Output $Name
            #Write-Output $($CLSID -eq $null)
            if ($CLSID -eq $null) {
                #Write-Output (Get-ChildItem -Path "HKCU:\Software\Classes\CLSID\" | Get-ItemProperty | Where-Object {$_."(default)" -eq $Name})
                $cloudProvider = Get-ChildItem -Path "HKCU:\Software\Classes\CLSID\" | Get-ItemProperty | Where-Object {$_."(default)" -eq $Name}
                $CLSID = $cloudProvider.PSChildName.Trim("{","}")
            }
            #Write-Output $cloudProvider.PSChildName.Trim("{","}")
            Write-Output "Removing Registry key"
            Write-Output "Removing HKCU:\Software\Classes\CLSID\{$CLSID}"
            Write-Output "Removing HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\NameSpace\{$CLSID}"
            Write-Output "Removing HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel\{$CLSID}"
            Write-Output "Removed"
        }
    }
}