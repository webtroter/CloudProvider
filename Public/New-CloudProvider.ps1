function New-CloudProvider {
    <#
    .Synopsis
      Creates the registry keys where the Cloud Provider is registered

    .Example
        C:\PS>New-CloudProvider -Name "OneCloud" -Path "%USERPROFILE%\OneCloud"
        
        This example creates a cloud provider OneCloud, with the root pointing to the UserProfile\OneCloud
    .Notes
        Name: New-CloudProvider
        Author: Alexis Vézina
        Last Edit: Date
    .Inputs
        None
    .Outputs
        None
    .PARAMETER Name
    The Name corresponds to the Name you see in the Windows Explorer

    #Requires -Version 2.0
    #>
    [CmdletBinding(SupportsShouldProcess=$True,DefaultParameterSetName="Default")]
    Param
    (
        [Parameter( Position=0,
            Mandatory=$true,
            HelpMessage="Name of the cloud Provider")]
            [string]$Name,

        [Parameter( Position=1,
            Mandatory=$true,
            HelpMessage="Where is the sync root?")]
        [string]$Path,

        # Parameter help description
        [Parameter( Mandatory=$false,
            HelpMessage="Icon Location, defaults to imageres.dll,69")]
        [string]$IconPath = "%SystemRoot%\system32\imageres.dll,69",

        [Parameter( Mandatory=$false,
            HelpMessage="Should we register the new Cloud Provider in the Sync Root Manager?",
            ParameterSetName="Manager")]
        <#[ValidateScript({
            if ($registerRootManager -and $ProviderID -ne "" -and $AccountID -ne "") {
                $True
            }
            else {
                throw "Please provide the Provider ID and Account ID when specifying RegisterRootManager"
            }})]#>
        [switch]$registerRootManager = $false,

        [Parameter( Mandatory=$true,
            HelpMessage="Cloud Provider ID",
            ParameterSetName="Manager")]
        [ValidateScript({
            if ($registerRootManager -and $ProviderID -ne "") {
                $True
            }
            else {
                throw "Please provide the Provider ID when specifying RegisterRootManager"
            }})]
        [string]$ProviderID,

        [Parameter(Mandatory=$true,
            HelpMessage="Cloud Account ID",
            ParameterSetName="Manager")]
        [ValidateScript({
            if ($registerRootManager -and $AccountID -ne "") {
                $True
            }
            else {
                throw "Please provide the Account ID when specifying RegisterRootManager"
            }})]
        [string]$AccountID
    )
    PROCESS {
        $LocalAccountSID = Get-WmiObject -Query "SELECT SID FROM Win32_UserAccount WHERE LocalAccount = 'True'" |Select-Object -First 1 -ExpandProperty SID
        $MachineSID      = ($p = $LocalAccountSID -split "-")[0..($p.Length-2)]-join"-"
        $folderName = $Name
        $winSID = $MachineSID
        $sourcepath = $Path
        $CLSID = [guid]::NewGuid()
        $regpathCU = "HKCU:\Software\Classes\CLSID\{$CLSID}"
        $regSyncRootManager = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\SyncRootManager"

        if ($pscmdlet.ShouldProcess("Continue?")) {
            #Write-Output "Doing it..."
            if ($psCmdlet.ParameterSetName -eq "Manager") {
                #Step 0 - Register in the Sync Root Manager
                New-Item -Path "$regSyncRootManager\$ProviderID!$winSID!$accountID" | out-null
                New-ItemProperty -Path "$regSyncRootManager\$ProviderID!$winSID!$accountID" -Name "DisplayNameResource" -Value $folderName -PropertyType String | out-null
                New-ItemProperty -Path "$regSyncRootManager\$ProviderID!$winSID!$accountID" -Name "IconResource" -Value $iconPath -PropertyType String | out-null
                New-Item -Path "$regSyncRootManager\$ProviderID!$winSID!$accountID\UserSyncRoots" | out-null
                New-ItemProperty -Path "$regSyncRootManager\$ProviderID!$winSID!$accountID\UserSyncRoots" -Name "$winSID" -Value "$sourcepath" -PropertyType String | out-null
            }
            #Step 1
            New-Item -Path $regpathCU -Value $folderName | out-null
            #Step 2
            new-Item -Path "$regpathCU\DefaultIcon" -Value $iconPath | out-null
            New-ItemProperty -Path "$regpathCU\DefaultIcon" -Name "(default)" -PropertyType ExpandString -Value $iconPath | out-null
            #Step 3
            New-ItemProperty -Path "$regpathCU" -Name "System.IsPinnedToNameSpaceTree" -PropertyType DWord -Value "0x1" | out-null
            #Step 4
            New-ItemProperty -Path "$regpathCU" -Name "SortOrderIndex" -PropertyType DWord -Value "0x39" | out-null
            #Step 5
            new-Item -Path "$regpathCU\InProcServer32" | out-null
            New-ItemProperty -Path "$regpathCU\InProcServer32" -Name "(default)" -PropertyType ExpandString -Value "%systemroot%\system32\shell32.dll" | out-null
            #Step 6
            new-Item -Path "$regpathCU\Instance" | out-null
            New-ItemProperty -Path "$regpathCU\Instance" -Name "CLSID" -PropertyType String -Value "{0E5AAE11-A475-4c5b-AB00-C66DE400274E}" | out-null
            #Step 7
            new-Item -Path "$regpathCU\Instance\InitPropertyBag" | out-null
            New-ItemProperty -Path "$regpathCU\Instance\InitPropertyBag" -Name "Attributes" -PropertyType DWord -Value "0x11" | out-null
            #Step 8
            New-ItemProperty -Path "$regpathCU\Instance\InitPropertyBag" -Name "TargetFolderPath" -PropertyType ExpandString -Value "$sourcepath" | out-null
            #Step 9
            new-Item -Path "$regpathCU\ShellFolder" | out-null
            New-ItemProperty -Path "$regpathCU\ShellFolder" -Name "FolderValueFlags" -PropertyType DWord -Value "0x28" | out-null
            #Step 10
            New-ItemProperty -Path "$regpathCU\ShellFolder" -Name "Attributes" -PropertyType DWord -Value "0xF080004D" | out-null
            #Step 11
            New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\NameSpace\{$CLSID}" -Value $folderName | out-null
            #Step 12
            New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -Name "{$CLSID}" -PropertyType DWord -Value "0x1" | out-null

        }
        else {
            
            Write-Output "Not doing it..."
            Write-Output $psCmdlet.ParameterSetName
            if ($psCmdlet.ParameterSetName -eq "Manager") { Write-Output "$ProviderID!$winSID!$accountID" }
        }
    }
} #End function